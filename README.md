This is code & note repo for `learnyoumongo` workshop.

## Important Aspects

### Install `mongodb` (exercise 01)

Download tarball from official site, unpack and place it as you wish. Then, add `mongodb` binary path to `PATH` environment variable.

### Start `mongodb` process (exercise 02)

Run

```sh
mongod --port 1234 --dbpath=./path/to/database
```

In this exercise

```sh
mongod --port 27017 --dbpath=./data
```

### Get a collection and operate on it!

After everything's set, we can connect to mongodb with MongoClient.

```js
mongo.connect('mongodb://host:port/dbName', (err, db) => {
  if (err) {
    console.log(err);
    return;
  }
  
  const collection = db.collection(collectionName);
  doSomethingWithcollection(collection);
});
```

After get the `collection` we want, we can do most of our jobs with it:

* Searching with `find` (exercise 03 and 04)
* Add data with `insert` (exercise 05)
* Update data with `update` (exercise 06)
* Remove data with `remove` (exercise 07)
* Count matched data with `count` (exercise 08)
* Do aggregate calculation with `aggregate` (exercise 09)
